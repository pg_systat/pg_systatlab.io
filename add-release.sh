#!/bin/sh

usage() {
	echo "usage: `basename $0` <source directory> <version>"
}

if [ $# -ne 2 ]; then
	usage
	exit 1
fi

SRCDIR=$1
VER=$2

NAME=pg_systat
RELDIR=`dirname $0`

# Check that we have all the programs we need.

which md5sum > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "md5sum missing"
	exit 1
fi

which sha256sum > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "sha256sum missing"
	exit 1
fi

# Check that all the files have been created.

APPIMAGE="${NAME}-${VER}-x86_64.AppImage"
PACKAGE="${NAME}-${VER}.tar.xz"

if [ ! -f "${SRCDIR}/build/appimage/${APPIMAGE}" ]; then
	echo "appimage missing: ${SRCDIR}/build/appimage/${APPIMAGE}"
	exit 1
fi

if [ ! -f "${SRCDIR}/build/source/${PACKAGE}" ]; then
	echo "appimage missing: ${SRCDIR}/build/source/${PACKAGE}"
	exit 1
fi

# Copy and generate sums.

cp -p ${SRCDIR}/build/appimage/${APPIMAGE} ${RELDIR}/appimage/
md5sum ${RELDIR}/appimage/${APPIMAGE} > ${RELDIR}/appimage/${APPIMAGE}.md5
sha256sum ${RELDIR}/appimage/${APPIMAGE} > ${RELDIR}/appimage/${APPIMAGE}.sha256
(cd ${RELDIR}/appimage && git add ${APPIMAGE} ${APPIMAGE}.md5 ${APPIMAGE}.sha256)

cp -p ${SRCDIR}/build/source/${PACKAGE} ${RELDIR}/source/
md5sum ${RELDIR}/source/${PACKAGE} > ${RELDIR}/source/${PACKAGE}.md5
sha256sum ${RELDIR}/source/${PACKAGE} > ${RELDIR}/source/${PACKAGE}.sha256
(cd ${RELDIR}/source && git add ${PACKAGE} ${PACKAGE}.md5 ${PACKAGE}.sha256)
