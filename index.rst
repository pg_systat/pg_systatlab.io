====================================================
 Welcome to the PostgreSQL systat Project Home Page
====================================================

.. image:: pg_systat.png
   :width: 75px
   :align: center

----

pg_systat is 'systat' for PostgreSQL.  It allows you to monitor various
PostgreSQL statistics tables from a terminal window.

Also see `pg_systat <https://pg_systat.gitlab.io/>`_ to monitor top PostgreSQL
processes.

.. image:: pg_systat-dbxact.png
   :width: 800px
   :alt: pg_systat screenshot

Download current releases:

.. list-table::
   :header-rows: 1

   * - Version
     - Download
     - Checksum
     - Release date
   * - 1.0.0
     - `.AppImage <appimage/pg_systat-1.0.0-x86_64.AppImage>`_
     - `md5 <appimage/pg_systat-1.0.0-x86_64.AppImage.md5>`__
       `sha256  <appimage/pg_systat-1.0.0-x86_64.AppImage.sha256>`__
     - 2020-10-08
   * -
     - `.xz <source/pg_systat-1.0.0.tar.xz>`_
     - `md5 <source/pg_systat-1.0.0.tar.xz.md5>`__
       `sha256  <source/pg_systat-1.0.0.tar.xz.sha256>`__
     - `Changes <https://gitlab.com/pg_systat/pg_systat/-/blob/main/HISTORY.rst>`_


`View the source code on-line: <https://gitlab.com/pg_systat/pg_systat>`_.

Download the source code with `git <https://www.git-scm.com/>`_::

    git clone git@gitlab.com:pg_systat/pg_systat.git

The PostgreSQL top project is a `PostgreSQL <https://www.postgresql.org>`_
Community project.
